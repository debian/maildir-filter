CC=gcc
CFLAGS=-O6 -pipe -g -Werror -Wall
LDFLAGS=-lpcre
BINNAME=maildir-filter

all: maildir-filter

install: all
	cp maildir-filter $(DESTDIR)/usr/bin/${BINNAME}
	chmod 755 $(DESTDIR)/usr/bin/${BINNAME}
	chown root.root $(DESTDIR)/usr/bin/${BINNAME}

update-templates:
	-chmod u+w po/maildir-filter.pot
	-xgettext -d maildir-filter -j -o po/maildir-filter.pot -k_ -T maildir-filter.c 

maildir-filter: maildir-filter.o

debian-package:
	cvs-buildpackage -F -rfakeroot

clean:
	rm -f *.o core *~ maildir-filter
