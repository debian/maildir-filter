#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pcre.h>
#include <errno.h>
#include <string.h>
#include <sys/utsname.h>
#include <time.h>
#include <libintl.h>

#define _(X) gettext(X)

#define BOUNCE		100
#define DEFERAL		111
#define STOP_PROCESSING	99
#define DONE_OK		EXIT_SUCCESS

#ifdef DEBUG
#define D(X) (X)
#else
#define D(X)
#endif

#define VERSION "$Id: maildir-filter.c,v 1.2 2004/07/14 04:25:07 mosca Exp $"

char **message=NULL;
char *directory=NULL;
int lines=0;
int kill=0;
int echo=0;
int debug=0;
int bounce=0;
int reg_opt=0;
int negated=0;
int headers_only=0;
pcre *regexp;
pcre_extra *reg_info;

void usage(int argc,char **argv);
void do_bounce();
void do_kill();
void do_deliver(const char *directory, unsigned long msg_len);
void version(int argc,char **argv);

int main(int argc,char **argv)
{
	char *line=NULL;
	int c;
	char *pattern=NULL;
	const char *err_msg=NULL;
	int err_offset;
	int matched=0;
	unsigned long msg_len=0;
	int in_headers=1;

	textdomain("maildir-filter");

	while((c=getopt(argc,argv,"r:kd:pDibVnH"))>0) {
		switch(c) {
			case 'r':
				pattern=optarg;
				break;
			case 'd':
				directory=optarg;
				break;
			case 'p':
				echo=1;
				break;
			case 'V':
				version(argc,argv);
				break;
			case 'D':
				debug=1;
				break;
			case 'i':
				reg_opt|=PCRE_CASELESS;
				break;
			case 'b':
				bounce=1;
				break;
			case 'n':
				negated=1;
				break;
			case 'H':
				headers_only=1;
				break;
			case 'k':
				kill = 1;
				break;
			default:
			case ':':
			case '?':
				if(c != ':' && c != '?')
					fprintf(stderr,_("Unkown option: %c\n"),c);
				usage(argc,argv);
				break;
		}
	}
	D(fprintf(stderr,"We are running..."));
	if(!pattern || (!directory && !bounce)) {
		D(fprintf(stderr,"pattern: %s\ndirectory: %s\nbounce: %i\n",pattern,directory,bounce));
		usage(argc,argv);
	}
		
	for(lines=0,line=malloc(BUFSIZ);line && fgets(line,BUFSIZ,stdin);lines++,line=malloc(BUFSIZ)) {
		size_t len = strlen(line);
		msg_len+=len;
		realloc(line, len+1);
		message=realloc(message,sizeof(void*) * lines+1);
		message[lines]=line;
	}

	if(!line || !message) {
		fprintf(stderr,_("%s: OOM: %s\n"),argv[0],strerror(errno));
		exit(DEFERAL);
	}

	regexp=pcre_compile((const char *)pattern,reg_opt,&err_msg,&err_offset,NULL);
	if(!regexp) {
		fprintf(stderr,_("Error compiling regexp: '%s' at character %i\n"),pattern,err_offset);
		fprintf(stderr,"%s\n",err_msg);
		exit(DEFERAL);
	} else {
		reg_info=pcre_study(regexp,0,&err_msg);
	}

	for(c=0;c<lines;c++) {
		int ret;

		if(echo) {
			fwrite(message[c],sizeof(char),strlen(message[c]),stdout);
		}

		if(in_headers && (message[c][0]=='\r' || message[c][0]=='\n')) {
			in_headers=0;
			if(debug) {
				fprintf(stderr,"headers end: %i\n",c+1);
			}
		}

		if(!matched) {
			if(!headers_only || (headers_only && in_headers)) {
				ret=pcre_exec(regexp,(const pcre_extra *)reg_info,message[c],strlen(message[c]),0,0 /* opt */,NULL,0);
				if(ret>=0) {
					fprintf(stderr, "%s : %s %i,", VERSION, _("we got a match at line"),c+1);
					if(bounce)
						fprintf(stderr,_(" bouncing\n"));
					else 
						fprintf(stderr, _(" delivering to %s. %lu bytes\n"), directory, msg_len);

					matched=1;
					if(!echo)
						break;
				} else if(ret<-1) {
					fprintf(stderr,_("Unexpect error matching string: %i\n"),ret);
					exit(DEFERAL);
				}
			}
		}
	}

	if(negated)
		matched = matched?0:1;

	if(matched) {
		if(bounce)
			do_bounce();
		else if(kill)
			do_kill();
		else
			do_deliver((const char *)directory, msg_len);
	}
	exit(DONE_OK);
}

void version(int argc,char **argv)
{
	fprintf(stderr,"%s\n", VERSION);
	fprintf(stderr, " by Marcelo Bezerra <mosca@mosca.yi.org>\n");
	exit(EXIT_SUCCESS);
}

void usage(int argc,char **argv)
{
	fprintf(stderr, _("Incorrect usage\n"));
	fprintf(stderr, _("%s -r <regexp> [-d ./Maildir/.folder | -b] [-D] [-p] [-i] [-V] [-n] [-H]\n"),argv[0]);
	fprintf(stderr, _("\t-b\tbounce if matches\n"));
	fprintf(stderr, _("\t-D\tturn on a few debug messages\n"));
	fprintf(stderr, _("\t-p\tprint message to stdout\n"));
	fprintf(stderr, _("\t-i\tcase insensitive match (m//i)\n"));
	fprintf(stderr, _("\t-V\tprint version and exit\n"));
	fprintf(stderr, _("\t-n\tnegate regexp. (!~ m//)\n"));
	fprintf(stderr, _("\t-k\tkill message (delete)\n"));
	fprintf(stderr, _("\t-H\tmatch headers only\n"));
	exit(DEFERAL);
}


void do_kill()
{
	exit(STOP_PROCESSING);
}

void do_bounce()
{
	exit(BOUNCE);
}

void do_deliver(const char *directory, unsigned long msg_len)
{
	char fname[BUFSIZ];
	char tmpname[BUFSIZ];
	struct utsname uts;
	int c;
	FILE *fp;
	time_t now;

	now=time(NULL);
	uname(&uts);
	snprintf(tmpname, BUFSIZ-1, "%s/tmp/%lu.%u.%s,S=%lu", directory, now, getpid(), uts.nodename, msg_len);
	snprintf(fname, BUFSIZ-1, "%s/new/%lu.%u.%s,S=%lu", directory, now, getpid(), uts.nodename, msg_len);

	fp=fopen(tmpname, "w");
	if(!fp) {
		perror("maildir-filter: do_deliver: fopen");
		exit(DEFERAL);
	}
	for(c=0;c<lines;c++) {
		fwrite(message[c],sizeof(char),strlen(message[c]),fp);
		//fprintf(fp,message[c]);
	}
	fclose(fp);
	if(rename(tmpname,fname)<0) {
		perror("maildir-filter: do_deliver: rename");
		exit(DEFERAL);
	}
	exit(STOP_PROCESSING);
}
